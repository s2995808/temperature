package nl.utwente.di.bookQuote;

public class Quoter {

    public Double getFahrenheit(String celsius) {
        return Double.parseDouble(celsius) * (9.0/5.0) + 32.0;
    }
}
